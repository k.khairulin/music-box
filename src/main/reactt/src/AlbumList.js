import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux'
import {loadAlbums} from "./actions/albumsActions";
import store from './store'

class AlbumList extends React.Component {

    componentDidMount(){
        if (this.props.albums.length === 0) {
            this.props.loadData();
        }
    }

    render() {
        /*const storage = store.getState()*/

        const albums = this.props.albums.map(album =>

            <div className="album">
            <h3>{album.title}</h3>
                <Link to={"/home/" + album.id}>
            <img className="album__cover" src={album.cover}/>
                </Link>
            </div>)
        return (
            <div className="home">
                {albums}
            </div>
        )
    }
}
const mapStateToProps = state => ({
    albums: state.albums,

});

const mapDispatchToProps = dispatch => ({
    loadData: () => dispatch(loadAlbums())
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumList);