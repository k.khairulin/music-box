import React from 'react';

class Login extends React.Component {

    sendData() {

        fetch('http://localhost:8080/api/login',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },

                body: JSON.stringify({usernameOrEmail:document.getElementById("email").value,
                password:document.getElementById("password").value})
            }).then(res => res.json())
            .then(res => localStorage.setItem("accessToken", res.accessToken))
            .then(res => window.location.href = "/home");
    }





    render() {


        return(
            <div className="form-login">
                <p className="welcome"><b>Welcome to MusicBox!</b></p>
                <input type="text" id="email" placeholder="E-mail"/>
                <input type="password" id="password" placeholder="Password"/>
                <button type="submit" onClick={() => this.sendData()}  class="submitbutton">Submit</button>
            </div>

        )
    }

}

export default Login;