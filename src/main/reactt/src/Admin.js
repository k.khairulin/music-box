import React from 'react';


class Admin extends React.Component {

    constructor(anyparams){
        super(anyparams)

        this.state = {
            album: {},
            albumCopy: {},
            albums: [],
            track: {},
            trackCopy: {},
            trackTitles: [],
            albumTitles: [],
            changeAlbum: "",
            changeTrack: "",
            addAlbum: "",
            addTrack: ""
        }

        this.getAlbums()
    }

    getAlbums = () =>
        fetch('http://localhost:8080/api/albums/',
        {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + localStorage.getItem("accessToken")
            },
        }).then(res=>res.json())
        .then((res) => this.setState({albums: res}))

    clear = () => {
        this.setState({changeAlbum:""},
            () => this.setState({albumTitles:[]},
                () => this.setState({trackTitles:[]})))
        document.getElementById("dropDowns").style.visibility="hidden"
        document.getElementById("trackDropDowns").style.visibility="hidden"
    }

    updateTrack = event => {
        event.preventDefault();
        const data = new FormData();
        if(document.querySelector('input[id="trackSource"]').files[0]){
            const filedata = document.querySelector('input[id="trackSource"]').files[0];
            data.append('file', filedata)
        }
        data.append('title', document.getElementById('trackTitle').value)

        fetch('http://localhost:8080/api/tracks/' + this.state.track.id,
            {
                method: 'PUT',
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem("accessToken")
                },
                body: data
            })
    }

    updateAlbum = event => {
        event.preventDefault();
        const data = new FormData();
        if(document.querySelector('input[id="cover"]').files[0]){
            const fileData = document.querySelector('input[id="cover"]').files[0];
            data.append('file', fileData)
        }

        data.append('title', document.getElementById('title').value);

        fetch('http://localhost:8080/api/albums/' + this.state.album.id,
            {
                method: 'PUT',
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem("accessToken")
                },

                body: data
            })
    }

    deleteTrack = () =>
        fetch('http://localhost:8080/api/tracks/' + this.state.track.id,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + localStorage.getItem("accessToken")
                },

            }).then(() => window.location="/admin")

    addAlbum = event => {
        event.preventDefault();
        const data = new FormData();
        const filedata = document.querySelector('input[id="albumCover"]').files[0];
        data.append('file', filedata);
        data.append('title', document.getElementById('albumTitle').value)

        fetch('http://localhost:8080/api/albums',
            {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem("accessToken")
                },
                body: data
            })
    }

    addTrack = event => {
        event.preventDefault();
        const data = new FormData();
        const filedata = document.querySelector('input[id="trackFile"]').files[0];
        data.append('file', filedata)
        data.append('title', document.getElementById('trackTitle').value)
        fetch('http://localhost:8080/api/tracks/' + this.state.album.id + '/song',
            {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem("accessToken")
                },
                body: data
            })
    }


    deleteAlbum = () =>
        fetch('http://localhost:8080/api/albums/' + this.state.album.id,
        {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + localStorage.getItem("accessToken")
            },

        }).then(() => window.location="/admin")


    deleteCover = () =>
        fetch("http://localhost:8080/api/albums/"+ this.state.album.id +"/cover", {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + localStorage.getItem("accessToken")
        }
    }).then(() => window.location="/admin")

    filter = () => {
        this.setState({trackTitles:[]})
        this.setState({changeTrack:""})
        this.setState({changeAlbum:""})
        let input  = document.getElementById("myInput");
        let filter = input.value.toUpperCase();
        if (input.value.length > 0){
            document.getElementById("dropDowns").style.visibility="visible"
            document.getElementById("trackDropDowns").style.visibility="visible"
        }else {
            document.getElementById("dropDowns").style.visibility="hidden"
            document.getElementById("trackDropDowns").style.visibility="hidden"
        }
        let albumTitles = [];
        let albs = this.state.albums.map(album =>
            <a href="#" className="album__dropdown"
                id={album.id}
                onClick=
                    {() => this.showAlbum(album.id)}>{album.title}</a>)

        for(let i = 0; i < albs.length && input.value; i++){
            if (albs[i].props.children.toUpperCase().indexOf(filter) > -1 ){
                albumTitles.push(albs[i]);
            }
        }
        this.setState({albumTitles:albumTitles})
    }
    showTrack = id => {
        this.setState({trackTitles:
                this.state.trackTitles.find(track => track.props.id === id)})

        this.setState({track: this.state.album.tracks.find(track => track.id === id)},

        this.setState({trackCopy:
                    this.state.album.tracks.find(track => track.id === id)},
            () => this.setState({changeTrack:
                    <div>
                        <form encType="multipart/form-data" onSubmit={this.updateTrack}>
                            <div>
                                <input className="input-track" type="text"
                                       defaultValue={this.state.trackCopy.title}
                                       id="trackTitle"/>
                                <p className='comment'>Specify new track title</p>
                            </div>
                            <div>
                                <input className="input-file" type="file"
                                       id="trackSource" name='Change file'/>
                                <p className='comment'>Add new file</p>
                            </div>
                            <div>
                                <button name='someName' className='admin-button'
                                        type='submit'>Change track</button>
                            </div>
                        </form>

                        <div>
                            <button className='delete-button' onClick=
                                        {() => this.deleteTrack()}>Delete track</button>
                        </div>

                        <div>
                            <button className='cancel-button' onClick=
                                        {() => this.setState({changeTrack:""},
                                        () => this.setState({trackTitles:[]},
                                        () => document.getElementById("trackDropDowns").style.visibility="hidden")
                                        )}>Cancel</button>
                        </div>
                    </div>})))


    }
    showAlbum = id => {
        document.getElementById("trackDropDowns").style.visibility = "visible"
        this.setState({albumTitles:
                this.state.albumTitles.find(album => album.props.id === id)})
        this.setState({album:
                this.state.albums.find(album => album.id === id)},
                () => this.setState({trackTitles:this.state.album.tracks.map
                    (track => <div className="track__dropdown"
                        id={track.id}
                        onClick={() => this.showTrack(track.id)}>
                            {track.title}
                            </div>)}))

        this.setState({albumCopy:
                    this.state.albums.find(album => album.id === id)},
            () => this.setState({changeAlbum:
                    <div>
                        <form encType="multipart/form-data" onSubmit={this.updateAlbum}>
                            <div>
                                <input className="input-album" type="text"
                                       defaultValue={this.state.albumCopy.title}
                                       id="title"/>
                                <p className='comment'>Specify new album title</p>
                            </div>
                            <div>
                                <input className="input-album" type="file"
                                       id="cover" name='Add a file'/>
                                <p className='comment'>Add new image</p>
                            </div>
                            <div>
                                <button className='admin-button' type='submit'
                                        name='updateAlbum'>Change album</button>
                            </div>
                        </form>

                        <div>
                            <button className='delete-button' onClick=
                                 {this.deleteAlbum}>Delete album</button>
                        </div>
                        <div>
                            <button className='delete-button' onClick=
                                 {this.deleteCover}>Delete cover</button>
                        </div>
                        <div>
                            <button className='cancel-button' onClick={() => this.clear()}>Cancel</button>
                        </div>
                     </div>}))
    }

    newAlbum = () => {
        this.setState({albumTitles:[]},() => this.setState({changeAlbum:""},
            this.setState({trackTitles:[]}, this.setState({changeTrack:""}))))
        this.setState({addAlbum:
            <div>
                <form encType="multipart/form-data" onSubmit={this.addAlbum}>
                    <input type="file" className="input-file" id="albumCover"/>
                    <input type="text" className="input-album"
                           id="albumTitle" placeholder="Album title" name="Add an image"/>
                    <button className='confirm-button' type='submit' name='submitAlbum'>Confirm new album</button>
                </form>
            </div>})
    }

    newTrack = () => {
        this.setState({albumTitles:[]},() => this.setState({changeAlbum:""},
            this.setState({trackTitles:[]}, this.setState({changeTrack:""}))))
        this.setState({addTrack:
            <div>
                <form encType="multipart/form-data" onSubmit={ event =>
                    this.setState({album:this.state.albums
                                .find(album =>
                                    album.title.toUpperCase() === document
                                        .getElementById("trackAlbum")
                                        .value.toUpperCase())},
                        () =>  this.addTrack(event))}>
                    <input type='file' className="input-file" id='trackFile' name='Add file'/>
                    <input type="text" className="input-track" id="trackTitle" placeholder="Track title"/>
                    <input type="text" className="input-track" id="trackAlbum" placeholder="Specify album"/>
                    <button type='submit' name="submitNew" className='confirm-button'>Confirm new track</button>
                </form>
            </div>})

    }


    render() {

        return (
            <div>
                <div className="dropdown">
                    <div id="myDropdown"
                         className="dropdown-content">
                        <input className="input-search" type="text"
                               placeholder="Search album"
                               id="myInput"
                               onChange={this.filter}/>

                        <div id="dropDowns" className="drop-downs">
                            {this.state.albumTitles}
                        </div>
                        <div>
                            {this.state.changeAlbum}
                        </div>
                        <div id="trackDropDowns" className="track-drop-downs">
                            {this.state.trackTitles}
                        </div>
                        <div>
                            {this.state.changeTrack}
                        </div>

                        <div className="addAlbum">
                            <button className='add-button' onClick={this.newAlbum}>New album</button>
                            {this.state.addAlbum}
                            <button className='add-button' onClick={this.newTrack}>New track</button>
                            {this.state.addTrack}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Admin;