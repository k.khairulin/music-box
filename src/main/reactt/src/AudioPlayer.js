import React from 'react'
import {connect} from 'react-redux'

class AudioPlayer extends React.Component {


    render() {

        return this.props.player.status==="START"
            ? <audio id="player" src={this.props.player.source} autoPlay controls></audio>
            : null

    }
}

const mapStateToProps = state => ({
    player: state.player
});


export default connect(mapStateToProps)(AudioPlayer)