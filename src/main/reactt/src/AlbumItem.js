import React from 'react';
import {connect} from 'react-redux'



class AlbumItem extends React.Component {
    constructor(anyparams){
        super(anyparams)

        this.state = {
            album: {},
            tracks: [],
            user:{}
        }
        this.getAcc()
    }

    getAcc = () => fetch('http://localhost:8080/api/users/me',
        {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + localStorage.getItem("accessToken")
            },
        }).then(res=>res.json())
        .then((res) => this.setState({user: res}))
        .then(() => this.getAlbum())

    isLoggedIn = function () {
        if(localStorage.getItem("accessToken")){
            return true
        }else {
            return false;
        }
    }

    addLike = id => fetch('http://localhost:8080/api/tracks/' + id + '/like',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + localStorage.getItem("accessToken")
            },
            body: JSON.stringify({user:this.props.user})
        })
    handleLike = id => {
        this.addLike(id)
        this.getAcc()
        setTimeout(function() { this.setState({tracks:this.state.album.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             clasName='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
                {this.isLoggedIn() && this.state.user.likes.find(like => like.track.id === track.id)?
                    <img className='like' src='https://i.imgur.com/WWJGKUi.png'/>:
                    this.isLoggedIn() && <img className='like' src='https://png.icons8.com/metro/1600/star.png'
                         onClick={() => this.handleLike(track.id)}/>}
            </div>)})}.bind(this), 100)

    }


    handleResume = () => {
        document.getElementById('player').play()
        this.setState({tracks:this.state.album.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             className='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
                {this.isLoggedIn() && this.state.user.likes.find(like => like.track.id === track.id)?
                    <img className='like' src='https://i.imgur.com/WWJGKUi.png'/>:
                    this.isLoggedIn() && <img className='like' src='https://png.icons8.com/metro/1600/star.png'
                         onClick={() => this.handleLike(track.id)}/>}
            </div>)})
    }

    handlePause = () => {
        document.getElementById('player').pause()
        this.setState({tracks:this.state.album.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             className='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
                {this.isLoggedIn() && this.state.user.likes.find(like => like.track.id === track.id)?
                    <img className='like' src='https://i.imgur.com/WWJGKUi.png'/>:
                    this.isLoggedIn() && <img className='like' src='https://png.icons8.com/metro/1600/star.png'
                         onClick={() => this.handleLike(track.id)}/>}
            </div>)})
    }

    playTrack = url => {
        this.props.playTrack(url)
        setTimeout(function() { this.setState({tracks:this.state.album.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             className='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
                {this.isLoggedIn() && this.state.user.likes.find(like => like.track.id === track.id)?
                    <img className='like' src='https://i.imgur.com/WWJGKUi.png'/>:
                    this.isLoggedIn() && <img className='like' src='https://png.icons8.com/metro/1600/star.png'
                         onClick={() => this.handleLike(track.id)}/>}
            </div>)})}.bind(this), 10)
    }

    getAlbum = () => fetch('http://localhost:8080/api/albums/' + this.props.id,
        {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + localStorage.getItem("accessToken")
            },
        }).then(res=>res.json())
        .then((res) => this.setState({album: res}))
        .then(() => this.setState({tracks:this.state.album.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             className='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
                {this.isLoggedIn() && this.state.user.likes.find(like => like.track.id === track.id)?
                    <img className='like' src='https://i.imgur.com/WWJGKUi.png'/>:
                    this.isLoggedIn() && <img className='like' src='https://png.icons8.com/metro/1600/star.png'
                         onClick={() => this.handleLike(track.id)}/>}
            </div>)}))


    render() {

        return (
            <div className="album">
                <h2>
                    {this.state.album.title}
                </h2>
                <img className="album__cover" src={this.state.album.cover}/>

                <div>
                    {this.state.tracks}
                </div>
            </div>
        )
    }

}

const mapDispatchToProps = dispatch => ({
    playTrack: url => dispatch({type: "PLAYER_START", payload: url}),

});

const mapStateToProps = state => ({
    player: state.player
});


export default connect(mapStateToProps, mapDispatchToProps)(AlbumItem)