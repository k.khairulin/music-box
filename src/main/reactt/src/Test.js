import React from 'react'

/*https://mybucket.s3-ap-southeast-1.amazonaws.com/myfilename*/

class Home extends React.Component {

    constructor(anyparams){
        super(anyparams)

        this.state = {
            albums: [],
            album: {}

        }
        this.getAlbums()
    }

    getAlbums = () =>
        fetch('http://localhost:8080/api/albums/',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + localStorage.getItem("accessToken")
                },
            }).then(res=>res.json())
            .then((res) => this.setState({albums: res}))

    sendRequest = event => {
        const albumTitle = document.getElementById('albumName').value
        this.setState({album:
                this.state.albums.find(album => album.title.toUpperCase() === albumTitle.toUpperCase())})
        this.sendFile(event)
    }

    sendFile = event => {
        event.preventDefault()
        const form = event.target;
        const data = new FormData();
        var filedata = document.querySelector('input[id="trackFile"]').files[0];
        data.append('file', filedata)
        data.append('title', document.getElementById('trackName').value)
        fetch('http://localhost:8080/api/tracks/' + this.state.album.id + '/song',
            {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem("accessToken")
                }, body: data
            })
    }


    render() {

        return(
            <div>
                <form encType="multipart/form-data" onSubmit={this.sendRequest}>
                    <input type='file' id='trackFile' name='file'/>
                    <input type='text' id='trackName' placeholder='specify track title'/>
                    <input type='text' id='albumName' placeholder='specify album title'/>
                    <input name='submit' type='submit'/>
                </form>
            </div>
        )
    }
}

export default Home