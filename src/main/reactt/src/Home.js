import React from 'react';
import AlbumList from './AlbumList'
import AlbumItem from './AlbumItem'
import {Route, Switch} from 'react-router-dom'
import {loadUser} from "./actions/userActions";
import {connect} from 'react-redux'





class Home extends React.Component {

    constructor(anyparams){
        super(anyparams)

        this.props.loadUser()
    }


    render() {

        const {match} = this.props;
        return (
            <div>
                <Switch>
                    <Route exact path={match.path} component={AlbumList}/>
                    <Route path={`${match.path}/:id/`}
                           render={props => <AlbumItem user={this.props.user} key={props.match.params.id} id={props.match.params.id}/>}/>
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user

});

const mapDispatchToProps = dispatch => ({
    loadUser: () => dispatch(loadUser())
});


export default connect(mapStateToProps, mapDispatchToProps)(Home)

