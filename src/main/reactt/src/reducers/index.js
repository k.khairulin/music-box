import {combineReducers} from 'redux';
import playerReducer from './playerReducer'
import albumsReducer from './albumsReducer'
import userReducer from "./userReducer";

export default combineReducers({
    player: playerReducer,
    albums: albumsReducer,
    user: userReducer
})