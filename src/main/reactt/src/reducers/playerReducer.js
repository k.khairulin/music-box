const initialState = {
    status: "STOP",
    source: ""
};



function playerReducer (state = initialState, action){
    switch(action.type) {
        case 'PLAYER_START':
            return {status:"START", source:action.payload};
        case "PLAYER_STOP":
            return{...state, status:"STOP"}
        default:
            return state;
    }
}

export default playerReducer