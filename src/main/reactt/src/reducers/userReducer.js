const initialState = {};

function userReducer (state = initialState, action){
    switch(action.type) {
        case 'USER_LOADED':
            return action.payload;
        default:
            return state;
    }
}

export default userReducer