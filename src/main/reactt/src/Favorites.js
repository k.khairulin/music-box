import React from 'react';
import {connect} from 'react-redux'

class Favorites extends React.Component {

    constructor(anyparams){
        super(anyparams)

        this.state = {
            user: {},
            tracks: [],
            view:[]
        }
        this.getTracks()
    }

    getTracks = () => fetch('http://localhost:8080/api/tracks/liked',
        {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + localStorage.getItem("accessToken")
            },
        }).then(res=>res.json())
        .then(res => this.setState({tracks: res}))
        .then(() => this.setState({view:this.state.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             className='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
            </div>)}))


    handleResume = () => {
        document.getElementById('player').play()
        this.setState({view:this.state.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             className='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
            </div>)})
    }

    handlePause = () => {
        document.getElementById('player').pause()
        this.setState({view:this.state.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             className='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
            </div>)})
    }

    playTrack = url => {
        this.props.playTrack(url)
        setTimeout(function() { this.setState({view:this.state.tracks.map(track =>
                <div className='tracks' key={track.id}>
                {this.props.player.source !== track.url?
                    <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                         className='play-button'
                         id={track.id} onClick={() => this.playTrack(track.url)}/>:
                    document.getElementById('player').paused &&
                    document.getElementById('player').duration > 0?
                        <img src='https://image.freepik.com/free-icon/play-button_318-42541.jpg'
                             className='play-button' onClick={() => this.handleResume()}/>:
                        <img src='https://image.freepik.com/free-icon/pause-button-outline_318-40569.jpg'
                             className='play-button' onClick={() => this.handlePause()}/>}
                <a className='track' href='#'>{track.title}</a>
            </div>)})}.bind(this), 10)
    }

    render() {
        console.log(this.state.view)

        return(
            <div>
                {this.state.view}
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    playTrack: url => dispatch({type: "PLAYER_START", payload: url}),

});

const mapStateToProps = state => ({
    player: state.player
});


export default connect(mapStateToProps, mapDispatchToProps)(Favorites)
