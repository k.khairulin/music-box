import {ALBUMS_LOADED} from './types';

export const loadAlbums = () => dispatch => {
    fetch('http://localhost:8080/api/albums/',
        {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + localStorage.getItem("accessToken")
            }
        }).then(res=>res.json())
        .then(data => dispatch({type:ALBUMS_LOADED, payload: data}))
}
