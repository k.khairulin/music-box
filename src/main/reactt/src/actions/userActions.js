import {USER_LOADED} from "./types";

export const loadUser =() => dispatch => {
    fetch('http://localhost:8080/api/users/me',
        {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + localStorage.getItem("accessToken")
            },
        }).then(res=>res.json())
        .then(data => dispatch({type:USER_LOADED, payload: data}))
}