import React from 'react';
import {Route, Link, Switch} from 'react-router-dom';
import Home from './Home';
import Login from './Login'
import Favorites from './Favorites'
import Admin from './Admin'
import AudioPlayer from './AudioPlayer'
import {connect} from 'react-redux'
import {loadAlbums} from "./actions/albumsActions";
import {loadUser} from "./actions/userActions";
import Test from './Test'


class App extends React.Component {

    constructor(anyparams){
        super(anyparams)

    }

    componentDidMount(){

        if (this.props.albums.length === 0) {
            this.props.loadData();
        }
        if(this.isLoggedIn()){
            this.props.loadUser();
        }

    }

    isLoggedIn = function () {
        if(localStorage.getItem("accessToken")){
            return true
        }else {
            return false;
        }
    }

    logout = () => fetch('http://localhost:8080/logout',
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(() => localStorage.removeItem("accessToken"))
        .then(() => window.location="/home")

    isAdmin = () =>  {
        return this.props.user.role === "ROLE_ADMIN"
    }



    render() {
        return (
            <div>
                <header className='header'>
                    <Link to="/home">
                        <img className="header__header-home" src="https://png.icons8.com/metro/1600/home.png"/>
                    </Link>
                    {(this.isLoggedIn()) &&
                    <Link to="/favorites">
                        <img className="header__header-fav" src="https://i.imgur.com/WWJGKUi.png"/>
                    </Link>}
                    <img className="header__header-logo" src="https://goodcode.io/static/images/content/work/musicbox-logo.png?rev=4f3f134"/>
                    {(!this.isLoggedIn()) &&
                    <Link to="/login">
                        <img className="header__header-login" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMkDVgJ76Ii0-NmOiAv7Nt-yrdmDHwRIK458s7fM6fOgBIWQE9"/>
                    </Link>}
                    { (this.isAdmin()) &&
                    <Link to="/admin">
                        <img className="header__header-icon" src="https://d30y9cdsu7xlg0.cloudfront.net/png/371299-200.png"/>
                    </Link> }
                    {this.isLoggedIn() &&
                    <img className="header__header-icon" onClick={() => this.logout()} src="https://cdn3.iconfinder.com/data/icons/mobiset-2/512/exit_delete_close_remove_door-512.png"/>}

                </header>
                <AudioPlayer/>
                <Switch>
                    <Route path="/test" component={Test}/>
                    <Route path="/home" component={Home}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/favorites" component={Favorites}/>
                    <Route path="/admin" component={Admin}/>
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.albums,
    user: state.user

});

const mapDispatchToProps = dispatch => ({
    loadData: () => dispatch(loadAlbums()),
    loadUser: () => dispatch(loadUser())
});


export default connect(mapStateToProps, mapDispatchToProps)(App)



