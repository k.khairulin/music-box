package com.musicbox.controller;

import com.musicbox.configuration.security.UserPrincipal;
import com.musicbox.model.Like;
import com.musicbox.model.Track;
import com.musicbox.model.User;
import com.musicbox.service.LikeService;
import com.musicbox.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class LikeController {

    @Autowired
    UserService userService;

    @Autowired
    LikeService likeService;

    @PostMapping(value = "/tracks/{id}/like")
    @ResponseBody
    @Transactional
    @CrossOrigin(value = "http://localhost:3000")
    public void addLike(@RequestBody Like like, @PathVariable Long id) {
        likeService.addLike(like, id);
    }

    @RequestMapping(value = "/tracks/liked", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin(value = "http://localhost:3000")
    public List<Track> getByUser() {
        UserPrincipal me = (UserPrincipal) SecurityContextHolder.
                getContext().
                getAuthentication().
                getPrincipal();
        User user = userService.getById(me.getUser_id());
        return likeService.getByUser(user);
    }
}
