package com.musicbox.controller;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.musicbox.config.Constants;
import com.musicbox.model.Album;
import com.musicbox.model.Track;
import com.musicbox.service.AlbumService;
import com.musicbox.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;
import java.io.InputStream;


@RestController
@RequestMapping("/api/tracks")
public class TrackController {

    @Autowired
    TrackService trackService;

    @Autowired
    AlbumService albumService;

    final AmazonS3 s3 = Constants.S3_BUILDER;
    final String bucket = Constants.BUCKET_NAME;

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @Transactional
    @RolesAllowed("ROLE_ADMIN")
    @CrossOrigin(value = "http://localhost:3000")
    public void removeById(@PathVariable Long id) {
        Track track = trackService.getById(id);
        if(track.getAmazon_key() != null) {
            String key = track.getAmazon_key();
            s3.deleteObject(bucket, key);
        }
        trackService.removeById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    @Transactional
    @RolesAllowed("ROLE_ADMIN")
    @CrossOrigin(value = "http://localhost:3000")
    public void updateTrack(@RequestParam(value = "file", required = false) MultipartFile file,
                            @RequestParam("title") String title,
                            @PathVariable Long id) throws IOException {

        Track track = trackService.getById(id);
        if (file != null) {
            String key = "tracks/" + file.getOriginalFilename();
            InputStream myFile = file.getInputStream();
            if (track.getAmazon_key() != null) {
                String oldKey = track.getAmazon_key();
                s3.deleteObject(bucket, oldKey);
            }
            s3.putObject(
                    bucket,
                    key,
                    myFile,
                    new ObjectMetadata());
            track.setAmazon_key(key);
            String url = s3.getUrl(bucket, key).toString();
            track.setUrl(url);
        }
        track.setTitle(title);
        trackService.updateTrack(track);
    }

    @PostMapping(value = "/{id}/song")
    @ResponseBody
    @Transactional
    @RolesAllowed("ROLE_ADMIN")
    @CrossOrigin(value = "http://localhost:3000")
    public void addTrack(@RequestParam("file") MultipartFile file,
                         @RequestParam("title") String title,
                         @PathVariable Long id) throws IOException {
        String key = "tracks/" + file.getOriginalFilename();
        InputStream myFile = file.getInputStream();
        s3.putObject(
                bucket,
                key,
                myFile,
                new ObjectMetadata());

        String url = s3.getUrl(bucket,key).toString();
        Track track = new Track();
        track.setTitle(title);
        Album album = albumService.getById(id);
        track.setAlbum(album);
        track.setUrl(url);
        track.setAmazon_key(key);
        trackService.addTrack(track);

    }

}
