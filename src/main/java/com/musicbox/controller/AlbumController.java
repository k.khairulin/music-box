package com.musicbox.controller;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.musicbox.config.Constants;
import com.musicbox.model.Album;
import com.musicbox.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
@RestController
@RequestMapping("/api/albums")
public class AlbumController {

    final AmazonS3 s3 = Constants.S3_BUILDER;
    final String bucket = Constants.BUCKET_NAME;

    @Autowired
    private AlbumService albumService;



    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin(value = "http://localhost:3000")
    public List<Album> getAll() {

        return albumService.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin(value = "http://localhost:3000")
    public Album getById(@PathVariable Long id){

        return albumService.getById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @RolesAllowed("ROLE_ADMIN")
    @Transactional
    @CrossOrigin(value = "http://localhost:3000")
    public void removeById(@PathVariable Long id) {
        Album album = albumService.getById(id);
        if(album.getAmazon_key() != null) {
            String key = album.getAmazon_key();
            s3.deleteObject(bucket, key);
        }
        albumService.removeById(id);


    }

    @RequestMapping(value = "/{id}/cover", method = RequestMethod.PUT)
    @ResponseBody
    @RolesAllowed("ROLE_ADMIN")
    @Transactional
    @CrossOrigin(value = "http://localhost:3000")
    public void removeCoverById(@PathVariable Long id) {
        Album album = albumService.getById(id);
        if(album.getAmazon_key() != null) {
            String key = album.getAmazon_key();
            s3.deleteObject(bucket, key);
        }

        albumService.removeCoverById(id);


    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ResponseEntity<String> testError() {

        return new ResponseEntity<>(AlbumService.testError(), HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    @Transactional
    @RolesAllowed("ROLE_ADMIN")
    @CrossOrigin(value = "http://localhost:3000")
    public void updateAlbum(@RequestParam(value = "file", required = false) MultipartFile file,
                            @RequestParam("title") String title, @PathVariable Long id) throws IOException {

        Album album = albumService.getById(id);
        if(file != null) {
            String key = "album_covers/" + file.getOriginalFilename();
            InputStream myFile = file.getInputStream();
            if(album.getAmazon_key() != null) {
                String oldKey = album.getAmazon_key();
                s3.deleteObject(bucket, oldKey);
            }
            s3.putObject(
                    bucket,
                    key,
                    myFile,
                    new ObjectMetadata());
            album.setAmazon_key(key);
            String url = s3.getUrl(bucket, key).toString();
            album.setCover(url);
        }
        album.setTitle(title);
        albumService.updateAlbum(album);


    }

    @PostMapping
    @ResponseBody
    @Transactional
    @RolesAllowed("ROLE_ADMIN")
    @CrossOrigin(value = "http://localhost:3000")
    public void addAlbum(@RequestParam("file") MultipartFile file,
                         @RequestParam("title") String title) throws IOException {

        String key = "album_covers/" + file.getOriginalFilename();
        InputStream myFile = file.getInputStream();
        s3.putObject(
                bucket,
                key,
                myFile,
                new ObjectMetadata());

        String url = s3.getUrl(bucket,key).toString();
        Album album = new Album();
        album.setTitle(title);
        album.setCover(url);
        album.setAmazon_key(key);
        albumService.addAlbum(album);

    }

}
