package com.musicbox.controller;

import com.musicbox.configuration.security.UserPrincipal;
import com.musicbox.model.User;
import com.musicbox.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAll() {
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/me")
    @ResponseBody
    @CrossOrigin(value = "http://localhost:3000")
    public User getMe() {
        UserPrincipal me = (UserPrincipal) SecurityContextHolder.
                getContext().
                getAuthentication().
                getPrincipal();
        User user = userService.getById(me.getUser_id());
        return user;
    }

}
