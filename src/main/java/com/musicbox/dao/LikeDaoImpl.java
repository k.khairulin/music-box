package com.musicbox.dao;

import com.musicbox.model.Like;
import com.musicbox.model.Track;
import com.musicbox.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.security.RolesAllowed;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class LikeDaoImpl implements LikeDao {

    @Autowired
    EntityManager entityManager;

    @Override
    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    public void addLike(Like like, Long id) {
        Track track = entityManager.find(Track.class, id);
        like.setTrack(track);
        entityManager.persist(like);
    }

    @Override
    public void removeById(Long id) {

    }

    @Override
    public Like getById(Long id) {
        return null;
    }

    @Override
    public List<Like> getAll() {
        return null;
    }

    @Override
    public List<Track> getByUser(User user) {
        List<Track> tracks = user.getLikes()
                .stream()
                .map(n -> n.getTrack())
                .collect(Collectors.toList());
        for (Track track:tracks
             ) {
            System.out.println(track.getTitle());
        }
        return tracks;
    }

}
