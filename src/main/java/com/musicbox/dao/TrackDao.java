package com.musicbox.dao;

import com.musicbox.model.Album;
import com.musicbox.model.Like;
import com.musicbox.model.Track;

import java.util.List;

public interface TrackDao {

    void updateTrack(Track track);

    void addTrack(Track track);

    void removeById(Long id);

    Track getById(Long id);

    List<Track> getAll();
}
