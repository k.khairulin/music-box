package com.musicbox.dao;

import com.musicbox.model.Album;
import com.musicbox.model.Like;
import com.musicbox.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.security.RolesAllowed;
import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class TrackDaoImpl implements TrackDao {

    @Autowired
    EntityManager entityManager;

    @Override
    public void updateTrack(Track track) {
        entityManager.merge(track);
    }

    @Override
    @RolesAllowed("ROLE_ADMIN")
    public void addTrack(Track track) {
        entityManager.persist(track);
    }

    @Override
    @RolesAllowed("ROLE_ADMIN")
    public void removeById(Long id) {
        Track track = entityManager.find(Track.class, id);
        entityManager.remove(track);
    }

    @Override
    public Track getById(Long id) {
        return entityManager.find(Track.class, id);
    }

    @Override
    public List<Track> getAll() {
        return null;
    }

}
