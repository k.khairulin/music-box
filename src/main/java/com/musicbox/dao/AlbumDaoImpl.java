package com.musicbox.dao;

import com.musicbox.model.Album;
import com.musicbox.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.security.RolesAllowed;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class AlbumDaoImpl implements AlbumDao {



    @Autowired
    private EntityManager entityManager;

    @Override
    public void addAlbum(Album album) {
        entityManager.persist(album);
    }

    @Override
    public void updateAlbum(Album album) {
        entityManager.merge(album);
    }

    @Override
    @Transactional
    public void removeById(Long id) {
        Album album = entityManager.find(Album.class, id);
        entityManager.remove(album);

    }

    @Override
    @RolesAllowed("ROLE_ADMIN")
    public void removeCoverById(Long id) {
        Album album = entityManager.find(Album.class, id);
        album.setCover(null);
        album.setAmazon_key(null);
        entityManager.merge(album);
    }

    @Override
    public Album getById(Long id) {
        return entityManager.find(Album.class, id);
    }

    @Override
    public List<Album> getAll() {

        Query query = entityManager.createNativeQuery("SELECT * FROM albums", Album.class);
        List<Album> list = (List<Album>) query.getResultList();
        return list;
    }

    @Override
    public List<Track> getTracksByAlbumId(Long id) {
        return null;
    }


}
