package com.musicbox.dao;

import com.musicbox.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class RoleDaoImpl implements RoleDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Role getById(Long id) {
        return null;
    }

    @Override
    public List<Role> getAll() {
        Query query = entityManager
                .createNativeQuery("SELECT * FROM roles", Role.class);
        List<Role> list = (List<Role>) query.getResultList();
        return list;
    }
}
