package com.musicbox.dao;

import com.musicbox.model.Album;
import com.musicbox.model.Like;
import com.musicbox.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void addUser(User user) {

    }

    @Override
    public User getById(Long id) {
        return entityManager.find(User.class, id);

    }

    @Override
    public User findByUsernameOrEmail(String username, String email) {
        String sql = "SELECT * FROM users WHERE LOGIN IN(\"" + username + "\")";

        Query query = entityManager
                .createNativeQuery(sql, User.class);
        User user = (User) query.getSingleResult();

        return user;
    }

    @Override
    public List<User> getAll() {

        return entityManager
                .createNativeQuery("SELECT * FROM USERS").getResultList();
    }

}
