package com.musicbox.service;

import com.musicbox.dao.TrackDao;
import com.musicbox.model.Like;
import com.musicbox.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    @Autowired
    TrackDao trackDao;



    @Override
    public void updateTrack(Track track) {
        trackDao.updateTrack(track);
    }

    @Override
    public void addTrack(Track track) {
        trackDao.addTrack(track);
    }

    @Override
    public void removeById(Long id) {
        trackDao.removeById(id);
    }

    @Override
    public Track getById(Long id) {
        return trackDao.getById(id);
    }

    @Override
    public List<Track> getAll() {
        return null;
    }
}
