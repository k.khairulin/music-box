package com.musicbox.service;

import com.musicbox.model.Role;

import java.util.List;

public interface RoleService {

    Role getById(Long id);

    List<Role> getAll();
}
