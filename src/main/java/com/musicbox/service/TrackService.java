package com.musicbox.service;

import com.musicbox.model.Album;
import com.musicbox.model.Like;
import com.musicbox.model.Track;
import org.springframework.stereotype.Service;

import java.util.List;


public interface TrackService {

    void updateTrack(Track track);

    void addTrack(Track track);

    void removeById(Long id);

    Track getById(Long id);

    List<Track> getAll();

}
