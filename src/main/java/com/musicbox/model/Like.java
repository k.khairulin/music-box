package com.musicbox.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "likes")
public class Like {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty
    private Long like_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    @JsonIgnoreProperties(value = "likes")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SONG_ID")
    @JsonIgnoreProperties(value = "likes")
    private Track track;

    public void setTrack(Track track) {
        this.track = track;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return like_id;
    }

    public void setId(Long id) {
        this.like_id = like_id;
    }

    public User getUser() {
        return user;
    }

    public Track getTrack() {
        return track;
    }
}
